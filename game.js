
var bgm = new Audio();
bgm.src = "DST.mp3";

var bep = new Audio();
bep.src = "pick.mp3";

var bop = new Audio();
bop.src = "blop.mp3";

var tok = new Audio();
tok.src = "Tok.mp3";

const choices = [9];
var indentifier = 0;
var moves = 0;

var figure = [document.getElementById("i1"),
			document.getElementById("i2"),
			document.getElementById("i3"),
			document.getElementById("i4"),
			document.getElementById("i5"),
			document.getElementById("i6"),
			document.getElementById("i7"),
			document.getElementById("i8"),
			document.getElementById("i9")];

var prevBox = 0;
var hasPick = false;
var turn = document.getElementById("turn");
var p1 = 0, p2 = 0;

var floor = [4];

floor[0] = document.getElementById('win1'); 
floor[1] = document.getElementById('win2');
floor[2] = document.getElementById('win1-3'); 
floor[3] = document.getElementById('win2-3');


var one1 = document.getElementById("score11");
var one2 = document.getElementById("score12");
var one3 = document.getElementById("score13");
var two1 = document.getElementById("score21");
var two2 = document.getElementById("score22");
var two3 = document.getElementById("score23");


class box {

	constructor() {
		this.val = "";
		this.status = false;
		this.picked = false;
	}

}


function gotAwinner(win){

	bop.play();
	floor[win].style.background = "black";
	floor[win].style.border= "10px solid gray";
	floor[win].style.width = "28.6%";
	floor[win].style.height = "55%";


	if(win === 0){
		if(p1===1){
			one1.style.width = "40px";
			one1.style.height = "45px";	
		}else if(p1===2){
			one2.style.width = "40px";
			one2.style.height = "45px";
		}else if(p1===3){
			one3.style.width = "40px";
			one3.style.height = "45px";
		}

	}else{
		if(p2===1){
			two1.style.width = "40px";
			two1.style.height = "45px";	
		}else if(p2===2){
			two2.style.width = "40px";
			two2.style.height = "45px";
		}else if(p2===3){
			two3.style.width = "40px";
			two3.style.height = "45px";
		}
	}

	if(p1 === 3){
		floor[2].style.background = "black";
		floor[2].style.border= "10px solid gray";
		floor[2].style.width = "28.6%";
		floor[2].style.height = "55%";
	}else if(p2===3){
		floor[3].style.background = "black";
		floor[3].style.border= "10px solid gray";
		floor[3].style.width = "28.6%";
		floor[3].style.height = "55%";
	}

}


function checkWinner(){

	if(choices[0].val === "X" && choices[0].val === choices[1].val && choices[0].val === choices[2].val){
		p1++;
		gotAwinner(0);
	}else if(choices[3].val === "X" && choices[3].val === choices[4].val && choices[3].val === choices[5].val){
		p1++;
		gotAwinner(0);
	}else if(choices[6].val === "X" && choices[6].val === choices[7].val && choices[6].val === choices[8].val){
		p1++;
		gotAwinner(0);
	}else if(choices[0].val === "X" && choices[0].val === choices[3].val && choices[0].val === choices[6].val){
		p1++;
		gotAwinner(0);
	}else if(choices[1].val === "X" && choices[1].val === choices[4].val && choices[1].val === choices[7].val){
		p1++;
		gotAwinner(0);
	}else if(choices[2].val === "X" && choices[2].val === choices[5].val && choices[2].val === choices[8].val){
		p1++;
		gotAwinner(0);
	}else if(choices[0].val === "X" && choices[0].val === choices[4].val && choices[0].val === choices[8].val){
		p1++;
		gotAwinner(0);
	}else if(choices[2].val === "X" && choices[2].val === choices[4].val && choices[2].val === choices[6].val){
		p1++;
		gotAwinner(0);
	}


	if(choices[0].val === "O" && choices[0].val === choices[1].val && choices[0].val === choices[2].val){
		p2++;
		gotAwinner(1);
	}else if(choices[3].val === "O" && choices[3].val === choices[4].val && choices[3].val === choices[5].val){
		p2++;
		gotAwinner(1);
	}else if(choices[6].val === "O" && choices[6].val === choices[7].val && choices[6].val === choices[8].val){
		p2++;
		gotAwinner(1);
	}else if(choices[0].val === "O" && choices[0].val === choices[3].val && choices[0].val === choices[6].val){
		p2++;
		gotAwinner(1);
	}else if(choices[1].val === "O" && choices[1].val === choices[4].val && choices[1].val === choices[7].val){
		p2++;
		gotAwinner(1);
	}else if(choices[2].val === "O" && choices[2].val === choices[5].val && choices[2].val === choices[8].val){
		p2++;
		gotAwinner(1);
	}else if(choices[0].val === "O" && choices[0].val === choices[4].val && choices[0].val === choices[8].val){
		p2++;
		gotAwinner(1);
	}else if(choices[2].val === "O" && choices[2].val === choices[4].val && choices[2].val === choices[6].val){
		p2++;
		gotAwinner(1);
	}


}





function clickedBox(boxNumber) {

	bgm.play();
	if(moves<6){
		if(indentifier%2==0){
			if(choices[boxNumber].status===false){
						
				choices[boxNumber].val = "X";
				choices[boxNumber].status = true;
				figure[boxNumber].src = "pics/Icon1_1.png";
				indentifier++;
				turn.src = "pics/Banner2.png";
				moves++;
			}
		}else{
			if(choices[boxNumber].status===false){
						
				choices[boxNumber].val= "O";
				choices[boxNumber].status = true;
				figure[boxNumber].src = "pics/Icon2_1.png";
				indentifier++;
				turn.src = "pics/Banner1.png";
				moves++;
			}
		}

		checkWinner()
	}else{

		if(choices[boxNumber].status===true && (indentifier%2===0) && choices[boxNumber].val==="X" && choices[boxNumber].picked !==true && hasPick === false){

			choices[boxNumber].picked = true;
			figure[boxNumber].src = "pics/Icon1_2.png";
			prevBox = boxNumber;
			hasPick = true;
		}else if(choices[boxNumber].status===true && (indentifier%2===1) && choices[boxNumber].val==="O" && choices[boxNumber].picked !==true && hasPick === false){

			choices[boxNumber].picked = true;
			figure[boxNumber].src = "pics/Icon2_2.png";
			prevBox = boxNumber;
			hasPick = true;
		}else if(choices[boxNumber].status === false && choices[prevBox].picked === true && (indentifier%2===0)){
			if(capable(prevBox,boxNumber)){

				choices[boxNumber].val = "X";
				choices[boxNumber].status = true;
				choices[boxNumber].picked = false;
				choices[prevBox].val = "";
				choices[prevBox].status = false;
				choices[prevBox].picked = false;
				figure[boxNumber].src = "pics/Icon1_1.png";
				figure[prevBox].src = "pics/logo.png";
				indentifier++;
				turn.src = "pics/Banner2.png";
				hasPick = false;
				checkWinner()
			}
		}else if(choices[boxNumber].status === false && choices[prevBox].picked === true && (indentifier%2===1)){
			if(capable(prevBox,boxNumber)){

				choices[boxNumber].val = "O";
				choices[boxNumber].status = true;
				choices[boxNumber].picked = false;
				choices[prevBox].val = "";
				choices[prevBox].status = false;
				choices[prevBox].picked = false;
				figure[boxNumber].src = "pics/Icon2_1.png";
				figure[prevBox].src = "pics/logo.png";
				indentifier++;
				turn.src = "pics/Banner1.png";
				hasPick = false;
				checkWinner()
			}
		}else if(choices[boxNumber]===choices[prevBox] && indentifier%2===0){
			choices[boxNumber].picked = false;
			figure[boxNumber].src = "pics/Icon1_1.png";
			hasPick = false;
		}else if(choices[boxNumber]===choices[prevBox] && indentifier%2===1){
			choices[boxNumber].picked = false;
			figure[boxNumber].src = "pics/Icon2_1.png";
			hasPick = false;
		}




	}
}



function capable(prevNum,currNum) {

	if(currNum === 0 && (prevNum === 1 || prevNum === 4 || prevNum === 3 )){
		return true;
	}else if(currNum === 1 && (prevNum === 0 || prevNum === 4 || prevNum === 2 )){
		return true;
	}else if(currNum === 2 && (prevNum === 1 || prevNum === 4 || prevNum === 5 )){
		return true;
	}else if(currNum === 3 && (prevNum === 0 || prevNum === 4 || prevNum === 6 )){
		return true;
	}else if(currNum === 4){
		return true;
	}else if(currNum === 5 && (prevNum === 2 || prevNum === 4 || prevNum === 8 )){
		return true;
	}else if(currNum === 6 && (prevNum === 3 || prevNum === 4 || prevNum === 7 )){
		return true;
	}else if(currNum === 7 && (prevNum === 6 || prevNum === 4 || prevNum === 8 )){
		return true;
	}else if(currNum === 8 && (prevNum === 7 || prevNum === 4 || prevNum === 5 )){
		return true;
	}else{
		return false;
	}

}



function playAgain(){


	for (var i = 0; i < 9; i++) {
		
		if(i<4){
			floor[i].style.background = "black";
			floor[i].style.border= "0px solid gray";
			floor[i].style.width = "0%";
			floor[i].style.height = "0%";
		}

		choices[i].val = "";
		choices[i].status = false;
		choices[i].picked = false;

		figure[i].src = "pics/logo.png";

	};

	moves=0;
	prevBox = 0;
	hasPick = false;

	if(p1===3 || p2 === 3){
		p1=0;
		p2=0;
		one1.style.width = "0px";
		one1.style.height = "0px";	
		one2.style.width = "0px";
		one2.style.height = "0px";	
		one3.style.width = "0px";
		one3.style.height = "0px";	
		two1.style.width = "0px";
		two1.style.height = "0px";	
		two2.style.width = "0px";
		two2.style.height = "0px";	
		two3.style.width = "0px";
		two3.style.height = "0px";
	}




}

for (var i = 0; i < 9; i++) {
	choices[i] = new box();
}


